/*
   ESP32 7-segment LED clock (3 LEDs per segment)
   WiFi to get current time (updates every hour) *Needs DST option
   Bluetooth serial to configure options
   EEPROM to store user options

   NOTE: Bluetooth/WiFi libraries are HUGE!!
         MUST set Tools->Partition Scheme to "Huge APP..."

   TODO:
    - Create hour change animations

   Sean Revels 1/13/2020
*/

#include <WiFi.h>             // WiFi connection
#include <FastLED.h>          // LED control
#include "time.h"
#include "BluetoothSerial.h"  // get configuration commands
#include <Preferences.h>      // store configuration options in EEPROM
#include <map>

#define ENABLE_ANIMATION_SUPPORT  0
#define TEST_ANIMATION  0

#define LED_DATA_PIN  13

#define SMALL_DESK_CLOCK  0
#if SMALL_DESK_CLOCK
#define NUM_LEDS  30          // 28 for 7-segments, 2 for colon
#else
#define NUM_LEDS  86          // 84 for 7-segments, 2 for colon
#endif

#define PREF_USE_ANIMATIONS    "UseAnimations"
#define PREF_FASTER_COLON      "FasterColon"
#define PREF_CLOCK_24HOUR      "Clock_24Hour"
#define PREF_DAY_HOUR          "DayHour"
#define PREF_DAY_MINUTE        "DayMinute"
#define PREF_NIGHT_HOUR        "NightHour"
#define PREF_NIGHT_MINUTE      "NightMinute"
#define PREF_DAY_BRIGHT        "DayBright"
#define PREF_NIGHT_BRIGHT      "NightBright"
#define PREF_TIME_ZONE_OFFSET  "tz_offset"
#define PREF_DAY_COLOR_R       "DayColor_r"
#define PREF_DAY_COLOR_G       "DayColor_g"
#define PREF_DAY_COLOR_B       "DayColor_b"
#define PREF_NIGHT_COLOR_R     "NightColor_r"
#define PREF_NIGHT_COLOR_G     "NightColor_g"
#define PREF_NIGHT_COLOR_B     "NightColor_b"
#define PREF_WIFI_SSID         "wifi_SSID"
#define PREF_WIFI_PASSWORD     "wifi_PASSWORD"
#define PREF_SHOW_DST_ACTIVE   "showDST"
#define PREF_CYCLE_TEST        "cycleTest"
#define PREF_ALL_RANDOM_COLORS "allRandom"
#define PREF_BLUETOOTH_NAME    "bluetoothName"
#define PREF_SHOW_LEADING_ZERO "leadingZero"

const char* ntpServer = "pool.ntp.org";
const int daylightOffset_sec = 3600;

#if SMALL_DESK_CLOCK
// map of LEDs to light to make each 7-segment shape
//     23          16         9         2  
//  24    22    17   15    10    8    3   1
//     27          20         13        6 
//  25    21    18   14    11    7    4   0 
//     26          19         12        5 


const std::map<int, int> LED_SHAPE = {
  { 0,  0x3F },
  { 1,  0x03 },
  { 2,  0x76 },
  { 3,  0x67 },
  { 4,  0x4B },
  { 5,  0x6D },
  { 6,  0x7D },
  { 7,  0x07 },
  { 8,  0x7F },
  { 9,  0x6F }
};
#define LEDS_PER_DIGIT    7
#define COLON_INDEX_1     28
#define COLON_INDEX_2     29
#else
// map of LEDs to light to make each 7-segment shape
//     71 70 69         50 49 48           29 28 27         8  7  6
//  72         68    51         47      30         26     9         5
//  73         67    52         46  84  31         25    10         4
//  74         66    53         45      32         24    11         3
//     83 82 81         62 61 60           41 40 39        20 19 18
//  75         65    54         44      33         23    12         2
//  76         64    55         43  85  34         22    13         1
//  77         63    56         42      35         21    14         0
//     78 79 80         57 58 59           36 37 38        15 16 17


const std::map<int, int> LED_SHAPE = {
  { 0,  0x03FFFF },
  { 1,  0x00003F },
  { 2,  0x1FF1F8 },
  { 3,  0x1F81FF },
  { 4,  0x1C0E3F },
  { 5,  0x1F8FC7 },
  { 6,  0x1FFFC7 },
  { 7,  0x0001FF },
  { 8,  0x1FFFFF },
  { 9,  0x1F8FFF }
};
#define LEDS_PER_DIGIT    21
#define COLON_INDEX_1     84
#define COLON_INDEX_2     85
#endif

// swipe left or right activating LEDs
const std::vector<int> LED_SWIPE_HORIZ_ANIM = {
  0x00003F,
  0x060040,
  0x090080,
  0x108100,
  0x007E00
};

// swipe up or down activating LEDs
const std::vector<int> LED_SWIPE_VERT_ANIM = {
  0x0001C0,
  0x000220,
  0x000410,
  0x000808,
  0x1C0000,
  0x001004,
  0x002002,
  0x004001,
  0x038000
};

TaskHandle_t Task1;           // Handle bluetooth COMs on 2nd CPU/thread
CRGB leds[NUM_LEDS];          // LED array
struct tm timeinfo;           // time from RTC
struct tm lastUpdateTime;     // last time time was received from server
int iHour = 0, iMinute = 0;   // Time currently shown

bool bRunCycleTest = false;
bool bUseAnimations = true;                       // animate hour change
bool bShow24HourClock = false;                    // 12 vs 24 hour clock
bool bFlashFasterColon = false;                   // colon blink faster when minute change approaches
bool bShowDSTActive = false;                      // Light colon different for DST
bool bUseRandomColors = false;                    // All pixels to random color
bool bShowLeadingZero = true;                     // Show hour leading zero
int iDayHour = 7, iNightHour = 21;                // color shift time
int iDayMinute = 0, iNightMinute = 0;             // color shift time
int iDayBrightness = 200, iNightBrightness = 25;  // brightness
int iTimeZoneOffset = -21600;                     // timezone offset (GMT -6)
CRGB dayColor = CRGB::Green;                      // LED color during day
CRGB nightColor = CRGB::DarkRed;                  // LED color during night
String sStatus = "Uninitialized";
String sWifiSSID = "";
String sWifiPassword = "";
String sBluetoothName = "ESP32_Clock";            // Name that shows during bluetooth connection

BluetoothSerial serialBT;

void _ResetToDefaults()
{
  bRunCycleTest = false;
#if ENABLE_ANIMATION_SUPPORT
  bUseAnimations = true;                       // animate hour change
#else
  bUseAnimations = false;                       // animate hour change
#endif
  bFlashFasterColon = false;
  bShow24HourClock = false;                    // 12 vs 24 hour clock
  bShowDSTActive = false;
  bUseRandomColors = false;
  bShowLeadingZero = true;
  iDayHour = 7;
  iNightHour = 21;                             // color shift time
  iDayMinute = 0;
  iNightMinute = 0;                             // color shift time
  iDayBrightness = 200;
  iNightBrightness = 25;                       // brightness
  iTimeZoneOffset = -21600;                    // timezone offset (GMT -6)
  dayColor = CRGB::Green;                      // LED color during day
  nightColor = CRGB::DarkRed;                  // LED color during night
}

void _LoadSettingsFromEEPROM()
{
  // initialize Preferences (EEPROM)
  Preferences pref;
  pref.begin("config", true);

#if ENABLE_ANIMATION_SUPPORT
  bUseAnimations = pref.getBool(PREF_USE_ANIMATIONS, true);
#endif

  bRunCycleTest = pref.getBool(PREF_CYCLE_TEST, false);
  bShow24HourClock = pref.getBool(PREF_CLOCK_24HOUR, false);
  bFlashFasterColon = pref.getBool(PREF_FASTER_COLON, false);
  bShowDSTActive = pref.getBool(PREF_SHOW_DST_ACTIVE, false);
  bUseRandomColors = pref.getBool(PREF_ALL_RANDOM_COLORS, false);
  bShowLeadingZero = pref.getBool(PREF_SHOW_LEADING_ZERO, true);

  iDayHour = pref.getInt(PREF_DAY_HOUR, 7);
  iDayMinute = pref.getInt(PREF_DAY_MINUTE, 0);
  iNightHour = pref.getInt(PREF_NIGHT_HOUR, 21);
  iNightMinute = pref.getInt(PREF_NIGHT_MINUTE, 0);
  iDayBrightness = pref.getInt(PREF_DAY_BRIGHT, 200);
  iNightBrightness = pref.getInt(PREF_NIGHT_BRIGHT, 25);
  iTimeZoneOffset = pref.getInt(PREF_TIME_ZONE_OFFSET, -21600);

  uint8_t r = pref.getUChar(PREF_DAY_COLOR_R, 0);
  uint8_t g = pref.getUChar(PREF_DAY_COLOR_G, 255);
  uint8_t b = pref.getUChar(PREF_DAY_COLOR_B, 0);

  dayColor = CRGB(r, g, b);

  r = pref.getUChar(PREF_NIGHT_COLOR_R, 128);
  g = pref.getUChar(PREF_NIGHT_COLOR_G, 0);
  b = pref.getUChar(PREF_NIGHT_COLOR_B, 0);
  nightColor = CRGB(r, g, b);

  //dayColor = (CRGB)pref.getInt(PREF_DAY_COLOR, CRGB::Green);
  //nightColor = (CRGB)pref.getInt(PREF_NIGHT_COLOR, CRGB::DarkRed);
  sWifiSSID = pref.getString(PREF_WIFI_SSID, String("Xante-E3"));
  sWifiPassword = pref.getString(PREF_WIFI_PASSWORD, String("Kp2514212207!"));
  sBluetoothName = pref.getString(PREF_BLUETOOTH_NAME, String("ESP32_Clock"));

  pref.end();
}

void _SaveSettingsToEEPROM()
{
  // initialize Preferences (EEPROM)
  Preferences pref;
  pref.begin("config");

#if ENABLE_ANIMATION_SUPPORT
  if (!pref.putBool(PREF_USE_ANIMATIONS, bUseAnimations))
    Serial.println("Error writing Animation");
#endif
  if (!pref.putBool(PREF_CYCLE_TEST, bRunCycleTest))
    Serial.println("Error writing Cycle Test");
  if (!pref.putBool(PREF_FASTER_COLON, bFlashFasterColon))
    Serial.println("Error writing Faster Colon");
  if (!pref.putBool(PREF_CLOCK_24HOUR, bShow24HourClock))
    Serial.println("Error writing Animation");
  if (!pref.putBool(PREF_SHOW_DST_ACTIVE, bShowDSTActive))
    Serial.println("Error writing DST Active");
  if (!pref.putBool(PREF_ALL_RANDOM_COLORS, bUseRandomColors))
    Serial.println("Error writing Random Color");
  if (!pref.putBool(PREF_SHOW_LEADING_ZERO, bShowLeadingZero))
    Serial.println("Error writing Leading Zero");

  if (!pref.putInt(PREF_DAY_HOUR, iDayHour))
    Serial.println("Error writing Day Hour");
  if (! pref.putInt(PREF_NIGHT_HOUR, iNightHour))
    Serial.println("Error writing Night Hour");
  if (!pref.putInt(PREF_DAY_MINUTE, iDayMinute))
    Serial.println("Error writing Day Minute");
  if (! pref.putInt(PREF_NIGHT_MINUTE, iNightMinute))
    Serial.println("Error writing Night Minute");
  if (!pref.putInt(PREF_DAY_BRIGHT, iDayBrightness))
    Serial.println("Error writing Day Bright");
  if (!pref.putInt(PREF_NIGHT_BRIGHT, iNightBrightness))
    Serial.println("Error writing Night Bright");
  if (!pref.putInt(PREF_TIME_ZONE_OFFSET, iTimeZoneOffset))
    Serial.println("Error writing Time Zone");

  pref.putUChar(PREF_DAY_COLOR_R, dayColor.r);
  pref.putUChar(PREF_DAY_COLOR_G, dayColor.g);
  pref.putUChar(PREF_DAY_COLOR_B, dayColor.b);

  pref.putUChar(PREF_NIGHT_COLOR_R, nightColor.r);
  pref.putUChar(PREF_NIGHT_COLOR_G, nightColor.g);
  pref.putUChar(PREF_NIGHT_COLOR_B, nightColor.b);

  if (!pref.putString(PREF_WIFI_SSID, sWifiSSID))
    Serial.println("Error writing WiFi SSID");
  if (!pref.putString(PREF_WIFI_PASSWORD, sWifiPassword))
    Serial.println("Error writing WiFi Password");
  if (!pref.putString(PREF_BLUETOOTH_NAME, sBluetoothName))
    Serial.println("Error writing Bluetooth Name");

  pref.end();
}

void printLocalTime()
{
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}

bool _IsDayTime()
{
  bool bDay = false;
  int iTempHour = timeinfo.tm_hour;
  int iTempMinute = timeinfo.tm_min;
  
  if( iTempHour == iDayHour )
  {
    // hour is same or greater, check minutes
    bDay = (iTempMinute >= iDayMinute);
  }
  else if ( iTempHour == iNightHour )
  {
    // hour is same as night time, check minutes
    bDay = !(iTempMinute >= iNightMinute );
  }
  else if ((iTempHour > iDayHour) && (iTempHour < iNightHour))
  {
    bDay = true;
  }

  return bDay;
}

void _ReadBluetoothCommands(void* param)
{
  while (true)
  {
    // check for commands from bluetooth connection
    if (serialBT.available())
    {
      // read from Serial bluetooth & report to serial output
      String data;
      while (serialBT.available())
        data += (char)serialBT.read();

      // process data
      if (!data.isEmpty())
      {
        bool bUpdateEEPROM = true;
        bool bUpdateDigits = false;
        switch (data[0])
        {
#if ENABLE_ANIMATION_SUPPORT
          case 'a':
          case 'A':
            {
              data.remove(0, 1);
              bUseAnimations = (data.toInt() == 1);

              String msg = "Animations: ";
              msg += bUseAnimations ? "ON" : "OFF";

              Serial.println(msg);
              serialBT.println(msg);
              break;
            }
#endif
          case 'b':
          case 'B':
            {
              switch (data[1])
              {
                case 'd':
                case 'D':
                  {
                    data.remove(0, 2); // remove control character
                    iDayBrightness = min(255L, max(1L, data.toInt()));
                    LEDS.setBrightness(iDayBrightness);
                    break;
                  }
                case 'n':
                case 'N':
                  {
                    data.remove(0, 2); // remove control character
                    iNightBrightness = min(255L, max(1L, data.toInt()));
                    LEDS.setBrightness(iNightBrightness);
                    break;
                  }
                default:
                  continue; // not valid
              }
              String msg = "Brightness: Day[";
              msg += String(iDayBrightness);
              msg += "] Night[";
              msg += String(iNightBrightness);
              msg += "]";

              Serial.println(msg);
              serialBT.println(msg);

              bUpdateDigits = true;
              break;
            }
          case 'C':
          case 'c':
            {
              bool bDay = false;
              switch (data[1])
              {
                case 'd':
                case 'D':
                  {
                    bDay = true;
                    break;
                  }
                case 'n':
                case 'N':
                  {
                    bDay = false;
                    break;
                  }
                default:
                  continue; // not valid
                  break;
              }
              data.remove(0, 2); // remove control character

              int iVals[3] = {0, 0, 0};
              int valIndex = 0;
              const char *ptr = data.c_str();

              // break comma delimeted #'s apart
              int index = 0;
              while (index > -1)
              {
                iVals[valIndex++] = min(255, max(0, atoi(ptr)));
                index = data.indexOf(',', index) + 1;
                if (valIndex > 2) break;
                ptr = data.c_str() + index;
              }

              // build a string to report to user
              String msg = "Color [";
              msg += bDay ? "Day] " : "Night] ";
              bool bFirst = true;
              for (const auto& val : iVals)
              {
                if (!bFirst) msg += ", ";
                bFirst = false;
                msg += String(val);
              }
              Serial.println(msg);
              serialBT.println(msg);

              if (bDay)
                dayColor = CRGB(iVals[0], iVals[1], iVals[2]);
              else
                nightColor = CRGB(iVals[0], iVals[1], iVals[2]);

              bUpdateDigits = true;
              break;
            }
          case 'd':
          case 'D':
            {
              data.remove(0, 1);
              if(data.indexOf(':') > 0)
              {
                iDayHour = min(24L, max(0L, data.substring(0, data.indexOf(':')).toInt()));
                iDayMinute = min(59L, max(0L, data.substring(data.indexOf(':') + 1).toInt()));
              }
              else {
                iDayHour = min(24L, max(0L, data.toInt()));
                iDayMinute = 0;
              }

              String msg = "Day time: ";
              msg += String(iDayHour);
              msg += ":";
              msg += String(iDayMinute);

              Serial.println(msg);
              serialBT.println(msg);

              bUpdateDigits = true;
              break;
            }
          case 'f':
          case 'F':
            {
              data.remove(0, 1);
              bFlashFasterColon = (data.toInt() == 1);

              String msg = "Colon blink: ";
              msg += bFlashFasterColon ? "FASTER" : "NORMAL";
              bUpdateDigits = true;

              Serial.println(msg);
              serialBT.println(msg);
              break;
            }
          case 'H':
          case 'h':
            {
              data.remove(0, 1); // remove control character
              sBluetoothName = data;
              sBluetoothName.trim();

              String msg = "Bluetooth Name: ";
              msg += sBluetoothName;

              Serial.println(msg);
              serialBT.println(msg);

              _SaveSettingsToEEPROM();
              delay(100); // give EEPROM time to write?

              ESP.restart();  // reboot to start new bluetooth name
              break;
            }
          case 'l':
          case 'L':
            {
              data.remove(0, 1);
              bShowLeadingZero = (data.toInt() == 1);

              String msg = "Show Leading Zero: ";
              msg += bShowLeadingZero ? "YES" : "NO";
              bUpdateDigits = true;

              Serial.println(msg);
              serialBT.println(msg);
              break;
            }
          case 'n':
          case 'N':
            {
              data.remove(0, 1);
              if(data.indexOf(':') > 0)
              {
                iNightHour = min(24L, max(0L, data.substring(0, data.indexOf(':')).toInt()));
                iNightMinute = min(59L, max(0L, data.substring(data.indexOf(':') + 1).toInt()));
              }
              else {
                iNightHour = min(24L, max(0L, data.toInt()));
                iNightMinute = 0;
              }

              String msg = "Night time: ";
              msg += String(iNightHour);
              msg += ":";
              msg += String(iNightMinute);

              Serial.println(msg);
              serialBT.println(msg);

              bUpdateDigits = true;
              break;
            }
          case 'r':
          case 'R':
            {
              data.remove(0, 1);
              bShow24HourClock = (data.toInt() == 1);
              String msg = "Clock Format: ";
              msg += bShow24HourClock ? "24-hour" : "12-hour";

              Serial.println(msg);
              serialBT.println(msg);

              bUpdateDigits = true;
              break;
            }
          case 's':
          case 'S':
            {
              data.remove(0, 1);
              bShowDSTActive = (data.toInt() == 1);
              String msg = "Show DST active: ";
              msg += bShowDSTActive ? "true" : "false";

              Serial.println(msg);
              serialBT.println(msg);
              break;
            }
          case 't':
          case 'T':
            {
              data.remove(0, 1);
              int iTZ = max(-12L, min(12L, data.toInt()));

              iTimeZoneOffset = iTZ * 3600; // offset in seconds

              String msg = "Time Zone: ";
              msg += String(iTZ);
              msg += " [";
              msg += String(iTimeZoneOffset);
              msg += "]";
              msg += " Restart clock to take affect [!]";

              Serial.println(msg);
              serialBT.println(msg);
              break;
            }
          case 'W':
          case 'w':
            {
              data.remove(0, 1); // remove control character

              // break comma delimeted strings apart
              int index = data.indexOf(',');
              if (index > -1)
              {
                sWifiSSID = data.substring(0, index);
                sWifiPassword = data.substring(index + 1);

                sWifiSSID.trim();
                sWifiPassword.trim();

                String msg = "WiFi: ";
                msg += sWifiSSID;
                msg += " => ";
                msg += sWifiPassword;

                Serial.println(msg);
                serialBT.println(msg);

                _SaveSettingsToEEPROM();
                delay(1000); // give EEPROM time to write?

                ESP.restart();  // reboot to connect with new WiFi credentials
              }
              break;
            }
          case 'x':
          case 'X':
            {
              data.remove(0, 1);
              bRunCycleTest = (data.toInt() == 1);

              String msg = "Cycle Test: ";
              msg += bRunCycleTest ? "ON" : "OFF";

              Serial.println(msg);
              serialBT.println(msg);

              _SaveSettingsToEEPROM();
              delay(1000); // give EEPROM time to write?

              ESP.restart();  // reboot
              break;
            }
          case 'z':
          case 'Z':
            {
              data.remove(0, 1);
              bUseRandomColors = (data.toInt() == 1);

              String msg = "Random Colors: ";
              msg += bUseRandomColors ? "ON" : "OFF";
              bUpdateDigits = true;

              Serial.println(msg);
              serialBT.println(msg);
              break;
            }
          case '#':
            {
              if (sStatus == "Running")
              {
                Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
                Serial.println(_IsDayTime() ? "Day Time!" : "Night Time");
                serialBT.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
                serialBT.println(_IsDayTime() ? "Day Time!" : "Night Time");
              }
              else
              {
                Serial.println(sStatus);
                serialBT.println(sStatus);
              }

              bUpdateEEPROM = false;
              break;
            }
          case '=':
            {
              String msg = "Settings:";

#if ENABLE_ANIMATION_SUPPORT
              // Show hour animations
              msg += "\r\n  [A]: ";
              msg += String(bUseAnimations);
#endif
              msg += "\r\n  [B]: [Day] ";
              msg += String(iDayBrightness);
              msg += " [Night] ";
              msg += String(iNightBrightness);

              msg += "\r\n  [C]: [Day] ";
              msg += String(dayColor.red);
              msg += "," + String(dayColor.green);
              msg += "," + String(dayColor.blue);
              msg += " [Night] ";
              msg += String(nightColor.red);
              msg += "," + String(nightColor.green);
              msg += "," + String(nightColor.blue);

              msg += "\r\n  [D]: ";
              msg += String(iDayHour);
              msg += ":";
              msg += String(iDayMinute);

              msg += "\r\n  [F]: ";
              msg += String(bFlashFasterColon);

              msg += "\r\n  [H]: ";
              msg += String(sBluetoothName);

              msg += "\r\n  [L]: ";
              msg += String(bShowLeadingZero);

              msg += "\r\n  [N]: ";
              msg += String(iNightHour);
              msg += ":";
              msg += String(iNightMinute);

              msg += "\r\n  [R]: ";
              msg += String(bShow24HourClock);

              msg += "\r\n  [S]: ";
              msg += String(bShowDSTActive);

              msg += "\r\n  [T]: ";
              msg += String(iTimeZoneOffset / 3600);
              msg += " [";
              msg += String(iTimeZoneOffset);
              msg += "]";

              msg += "\r\n  [W]: ";
              msg += sWifiSSID;

              msg += "\r\n  [Z]: ";
              msg += String(bUseRandomColors);

              Serial.println(msg);
              serialBT.println(msg);
              bUpdateEEPROM = false;
              break;
            }
          case '*':
            _ResetToDefaults();
            Serial.println("Reset to default values");
            serialBT.println("Reset to default values");
            bUpdateDigits = true;
            break;
          case '?':
            {
              String msg(
#if ENABLE_ANIMATION_SUPPORT
                "a/A: Use Hour transition animation (0/1)\r\n"
#endif
                "b/B[d/n]1-255: change brightness\r\n"
                "   d# - Set daytime brightness\r\n"
                "   n# - Set night time brightness\r\n"
                "c/C[d/n]#,#,#: change RGB color\r\n"
                "   d#,#,# - Set RGB daytime color\r\n"
                "   n#,#,# - Set RGB night time color\r\n"
                "d/D: set daytime (0-24):(0-59)\r\n"
                "f/F: colon blinks faster closer to 60 secs (0/1)\r\n"
                "h/H: set Bluetooth Device Name\r\n"
                "l/L: show leading zero for hours (0/1)\r\n"
                "n/N: set night time (0-24):(0-59)\r\n"
                "r/R: use 24 hour readout (0-1)\r\n"
                "s/S: colon is white for daylight savings time\r\n"
                "t/T: set timezone (-12 - +12))\r\n"
                "w/W: set 2.4Ghz Wifi (SSID,PASSWORD)\r\n"
                "x/X: test LEDs instead of showing time (0-1)\r\n"
                "z/Z: Use random color for time (0-1)\r\n"
                "#: Current Status/Time\r\n"
                "!: Reboot controller\r\n"
                "*: Reset configuration to defaults\r\n"
                "=: Show all current values\r\n"
                "?: Show help\r\n");
              serialBT.println(msg);
              bUpdateEEPROM = false;
              break;
            }
          case '!':         // force reboot
            ESP.restart();
            bUpdateEEPROM = false;
            break;
          default:
            // write command received to serial term for debugging
            serialBT.print("Unknown command: ");
            serialBT.println(data);
            Serial.print("Unknown command: ");
            Serial.println(data);
            bUpdateEEPROM = false;
            break;
        }

        // refresh time
        if (bUpdateDigits) _UpdateDigits();

        // if EEPROM value updated, update EEPROM
        if (bUpdateEEPROM) _SaveSettingsToEEPROM();
      }
    }
    delay(100);
  }
}

void setup() {
  randomSeed(analogRead(0));

  sStatus = "Loading";

  /* load default values from EEPROM */
  _LoadSettingsFromEEPROM();

  // prepare WiFi
  WiFi.disconnect(true);
  WiFi.mode(WIFI_STA);
  delay(3000);

  // configure serial port for feedback
  Serial.begin(9600);
  Serial.println(sBluetoothName);
  serialBT.begin(sBluetoothName);

  // Start bluetooth task
  xTaskCreatePinnedToCore(
    _ReadBluetoothCommands, /* Task function. */
    "Task1",                /* Name of task. */
    10000,                  /* Stack size in words */
    NULL,                   /* input param. */
    0,                      /* priority */
    &Task1,                 /* task handle */
    0);                     /* Core to run task. */

  // configure LED array for 4 7-segments displays, and 2 LEDs for colon
  LEDS.addLeds<WS2812B, LED_DATA_PIN, GRB> (leds, NUM_LEDS);
  LEDS.setBrightness(iNightBrightness);

  if (!bRunCycleTest)
  {
    int iLED = 0;
    int status = WL_IDLE_STATUS;

    sStatus = "Connecting";

    // attempt to connect to Wifi network:
    while ( status != WL_CONNECTED) {
      Serial.print("Attempting to connect to network: ");
      Serial.println(sWifiSSID);
      status = WiFi.begin((const char*)sWifiSSID.c_str(), (const char*)sWifiPassword.c_str());

      // move green dot to show we're working...
      leds[iLED] = CRGB::Green;
      if (iLED > 0) leds[iLED - 1] = CRGB::Black;
      else if (iLED == 0) leds[NUM_LEDS - 1] = CRGB::Black;

      iLED++;
      if (iLED > NUM_LEDS) iLED = 0;
      FastLED.show();

      // wait 5 seconds for connection:
      delay(5000);
    }

    sStatus = "Connected";

    // get time from network & save to RTC
    configTime(iTimeZoneOffset, daylightOffset_sec, ntpServer);

    // print local time, necessary to get synced before disconnecting WiFi?
    printLocalTime();

    // store current time from server
    lastUpdateTime = timeinfo;

    WiFi.disconnect(true);
    WiFi.mode(WIFI_OFF);
  }

  // change all LEDs to black
  memset(leds, CRGB::Black, NUM_LEDS);
  FastLED.show();

  sStatus = "Running";
}

// Put number at placement
void _SetDigit(const int placement, const int number, const bool bDay)
{
  // make sure values are valid
  if ( !((placement >= 0) && (placement < 4))) return;
  if ( !((number >= 0) && (placement < 10))) return;

  // set number into placement (0, 1, 2, 3) 0 being lowest digit
  int ledShape = LED_SHAPE.at(number);
  int startIndex = 0;
  int index = startIndex = (placement * LEDS_PER_DIGIT);

  // select color based on time of day and options
  CRGB Color = bDay ? dayColor : nightColor;
  if (bUseRandomColors) Color = CRGB(random(256), random(256), random(256));
  if (!bShowLeadingZero && ((placement == 3) && (number == 0))) Color = CRGB::Black;

  // iterate each individual LED and toggle
  int led = 0;
  for (int j = startIndex; j < (startIndex + LEDS_PER_DIGIT); j++)
  {
    // if bit is set, activate LED, else turn off
    if (((1 << led) & ledShape))
    {
      leds[index] = Color;
    }
    else leds[index] = CRGB::Black;
    index++;
    led++;
  }
}

void _UpdateDigits()
{
  //printLocalTime();

 // update LED clock digits
  int iTempHour = timeinfo.tm_hour;
  int iTempMinute = timeinfo.tm_min;

  bool bDay = _IsDayTime();
  
  if (bDay)
    LEDS.setBrightness(iDayBrightness);
  else
    LEDS.setBrightness(iNightBrightness);

  bool bAnimate = bUseAnimations && (iTempHour != iHour);

  iHour = iTempHour;
  iMinute = iTempMinute;
  // convert 24 hour to 12
  if (!bShow24HourClock)
  {
    if (iTempHour == 0) iTempHour = 12;
    else if (iTempHour > 12) iTempHour -= 12;
  }

  // split time 23 = (2 & 3)
  int t[4] = {
    (iMinute % 10),
    ((iMinute / 10) % 10),
    (iTempHour % 10),
    ((iTempHour / 10) % 10),
  };

#if ENABLE_ANIMATION_SUPPORT
  if ( bAnimate )
  {
    // using animations and hour changed, animate time change
    for (const auto& column : LED_SWIPE_HORIZ_ANIM)
    {

    }
  }
  else
#endif
  {
    /* Iterate each 7-segment display */
    for ( int i = 0; i < 4; i++ )
    {
      _SetDigit(i, t[i], bDay);
    }
  }
}

bool bLEDON = false;
void _BlinkColon()
{
  bool bDay = _IsDayTime();

  // blink colon LEDs
  CRGB Color = !bDay ? nightColor : dayColor;
  if (bShowDSTActive && !bDay && timeinfo.tm_isdst) Color = CRGB::White;
  else if (bUseRandomColors) Color = CRGB(random(256), random(256), random(256));

  leds[COLON_INDEX_1] = leds[COLON_INDEX_2] = bLEDON ? Color : CRGB::Black;  // set LED color
  bLEDON = !bLEDON;
}

// put your main code here, to run repeatedly:
int iCycleNumber[4] = {0, 1, 2, 3};
void loop() {

  int iDelay = 1000;

#if TEST_ANIMATION // test animation
  //                                                             i i-1
  //     71 70 69         50 49 48           29 28 27         8  *  |
  //  72         68    51         47      30         26     9         5
  //  73         67    52         46  84  31         25    10         4
  //  74         66    53         45      32         24    11         3
  //     83 82 81         62 61 60           41 40 39        20  *  |
  //  75         65    54         44      33         23    12         2
  //  76         64    55         43  85  34         22    13         1
  //  77         63    56         42      35         21    14         0
  //     78 79 80         57 58 59           36 37 38        15  *  |

  // iterate each of the 4 digits
  for (int iDigit = 0; iDigit < 4; iDigit++)
  {
    // iterate each section of animation
    for (int iColumn = 0; iColumn < LED_SWIPE_HORIZ_ANIM.size() + 2; iColumn++)
    {
      // iterate all 21 LEDs for this number
      for (int iLED = 0; iLED < 21; iLED++)
      {
        if (iColumn < LED_SWIPE_HORIZ_ANIM.size())
        {
          // set i as black
          if ((1 << iLED) & LED_SWIPE_HORIZ_ANIM[iColumn])
            leds[iLED + (iDigit * 21)] = CRGB::Black;
        }

        // set to WHITE if needed
        if (iColumn - 1 >= 0)
        {
          // set i as green
          if (((1 << iLED) & LED_SWIPE_HORIZ_ANIM[iColumn - 1]))
            leds[iLED + (iDigit * 21)] = CRGB::White; // turn on
        }

        // set to green if needed
        if (iColumn - 2 >= 0)
        {
          int ledShape = LED_SHAPE.at(iCycleNumber[iDigit]);
          // set i as green
          if (((1 << iLED) & LED_SWIPE_HORIZ_ANIM[iColumn - 2]) && ((1 << iLED) & ledShape))
            leds[iLED + (iDigit * 21)] = CRGB::Green; // turn on
          else if (((1 << iLED) & LED_SWIPE_HORIZ_ANIM[iColumn - 2]))
            leds[iLED + (iDigit * 21)] = CRGB::Black;
        }
      }
      _BlinkColon();

      FastLED.show(); // refresh LEDs

      // don't delay for last column of number
      if (iColumn < LED_SWIPE_HORIZ_ANIM.size() - 1) delay(300);
    }
  }
  delay(400);

  // move to next number
  iCycleNumber[3] = iCycleNumber[2];
  iCycleNumber[2] = iCycleNumber[1];
  iCycleNumber[1] = iCycleNumber[0];
  iCycleNumber[0]++;
  if (iCycleNumber[0] > 9) iCycleNumber[0] = 0;
#else
  if (bRunCycleTest)
  {
    // lowest position
    for ( int i = 0; i < 4; i++)
      _SetDigit(i, iCycleNumber[i], true);

    // increment number until > 9, then cycle back to 0
    iCycleNumber[3] = iCycleNumber[2];
    iCycleNumber[2] = iCycleNumber[1];
    iCycleNumber[1] = iCycleNumber[0];
    iCycleNumber[0]++;
    if (iCycleNumber[0] > 9) iCycleNumber[0] = 0;
  }
  else
  {
    if (getLocalTime(&timeinfo))
    {
      //if (iMinute != timeinfo.tm_min)
      {
        _UpdateDigits();
      }

      // update colon color
      _BlinkColon();

      if (bFlashFasterColon)
      {
        if (timeinfo.tm_sec >= 50) iDelay = 250;
        else if (timeinfo.tm_sec >= 40) iDelay = 500;
      }
    }
    else
    {
      Serial.println("getLocalTime failed!");
    }
  }
  // update LEDs
  FastLED.show();
#endif

  // sleep to blink the colon
  delay(iDelay);
}
